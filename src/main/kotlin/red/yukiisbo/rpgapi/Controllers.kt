package red.yukiisbo.rpgapi

import org.springframework.data.repository.findByIdOrNull
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import java.util.*

data class APIResponse(val error: Int, val data: Any)

@ControllerAdvice
class ExceptionAdvice {
    @ExceptionHandler
    fun handleException(e: Exception) : ResponseEntity<APIResponse> =
            ResponseEntity(
                    APIResponse(1, e.message ?: "An unknown error has occurred"),
                    HttpStatus.BAD_REQUEST
            )
}

@RestController
@RequestMapping("/items")
class ItemController(private val repository: ItemRepository) {
    @GetMapping("/get")
    fun all() = repository.findAll()

    @PostMapping("/create")
    fun create(@RequestBody item: Item): APIResponse {
        val _item = Item(
                name = item.name,
                type = item.type,
                price = item.price,
                effect = item.effect
        )
        repository.save(_item)
        return APIResponse(0, _item)
    }
}

data class SlotUpdateData(val id: UUID, val slotName: String, val items: List<Item>)

@RestController
@RequestMapping("/persos")
class PersonController(private val repository: PersonRepository,
                       private val itemRepository: ItemRepository) {
    @GetMapping("/get")
    fun all() = repository.findAll()

    @PostMapping("/create")
    fun create(@RequestBody person: Person) : APIResponse {
        val _person = Person(
                name = person.name,
                level = person.level,
                gold = person.gold,
                life = person.life,
                vitality = person.vitality,
                strength = person.strength,
                armor = person.armor
        )
        repository.save(_person)
        return APIResponse(0, _person)
    }

    @RequestMapping(method = [RequestMethod.PUT], value = ["/updateslot"])
    fun updateSlot(@RequestBody data: SlotUpdateData) : APIResponse {
        val p = repository.findByIdOrNull(data.id) ?: throw Exception("Player not found")

        val slot = p.slots
                .firstOrNull { it.name == data.slotName } ?: throw Exception("Slot not found")

        data.items
                .map { it._id ?: throw Exception("Item ID is null") }
                .map { itemRepository.findByIdOrNull(it) }
                .map { it ?: throw Exception("Item not found") }
                .forEach { slot.items.add(it) }

        repository.save(p)

        return APIResponse(0, "ok")
    }
}

@RestController
@RequestMapping("/streets")
class StreetController(private val repository: StreetRepository) {
    @GetMapping("/get")
    fun all() = repository.findAll()

    @PostMapping("/create")
    fun create(@RequestBody street: Street) : APIResponse {
        val _street = Street(name = street.name)
        repository.save(_street)
        return APIResponse(0, _street)
    }
}

data class ShopCreateData(val id: UUID, val name: String, val type: String)
data class ShopAddItemData(val idShop: UUID, val idItem: UUID)

@RestController
@RequestMapping("/shops")
class ShopController(private val repository: ShopRepository,
                     private val streetRepository: StreetRepository,
                     private val itemRepository: ItemRepository) {
    @PostMapping("/create")
    fun create(@RequestBody data: ShopCreateData) : APIResponse {
        val s = streetRepository.findByIdOrNull(data.id) ?: throw Exception("Street not found")

        val _shop = Shop(name = data.name, type = data.type)

        s.shops.add(_shop)
        streetRepository.save(s)

        return APIResponse(0, _shop)
    }

    @RequestMapping(method = [RequestMethod.PUT], value = ["/additem"])
    fun addItem(@RequestBody data: ShopAddItemData) : APIResponse {
        val s = repository.findByIdOrNull(data.idShop) ?: throw Exception("Shop not found")
        val i = itemRepository.findByIdOrNull(data.idItem) ?: throw Exception("Item not found")

        s.items.add(i)
        repository.save(s)

        return APIResponse(0, "ok")
    }
}