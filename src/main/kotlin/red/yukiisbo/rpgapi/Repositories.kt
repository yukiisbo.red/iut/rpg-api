package red.yukiisbo.rpgapi

import org.springframework.data.repository.CrudRepository
import java.util.*

interface ItemRepository : CrudRepository<Item, UUID>
interface SlotRepository : CrudRepository<Slot, UUID>
interface PersonRepository : CrudRepository<Person, UUID>
interface ShopRepository : CrudRepository<Shop, UUID>
interface StreetRepository : CrudRepository<Street, UUID>