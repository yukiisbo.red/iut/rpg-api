package red.yukiisbo.rpgapi

import org.hibernate.annotations.GenericGenerator
import java.math.BigDecimal
import java.util.*
import javax.persistence.*
import kotlin.collections.ArrayList

@Entity
class Item(
        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        var _id: UUID? = null,
        var name: String,
        var type: String,
        var price: BigDecimal,
        var effect: String
)

@Entity
class Slot(
        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        var _id: UUID? = null,
        var name: String,
        @OneToMany(cascade = [CascadeType.ALL])
        var items: MutableList<Item> = mutableListOf()
)

@Entity
class Person(
        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        var _id: UUID? = null,
        var name: String,
        var level: Int,
        var gold: BigDecimal,
        var life: Int,
        var vitality: Int,
        var strength: Int,
        var armor: Int,
        @OneToMany(cascade = [CascadeType.ALL])
        var slots: List<Slot> =
                listOf(
                        Slot(name = "head"),
                        Slot(name = "body"),
                        Slot(name = "hands"),
                        Slot(name = "belt"),
                        Slot(name = "bag")
                )
)

@Entity
class Shop(
        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        var _id: UUID? = null,
        var name: String,
        var type: String,
        @OneToMany(cascade = [CascadeType.ALL])
        var items: MutableList<Item> = mutableListOf()
)

@Entity
class Street(
        @Id
        @GeneratedValue(generator = "UUID")
        @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
        var _id: UUID? = null,
        var name: String,
        @OneToMany(cascade = [CascadeType.ALL])
        var shops: MutableList<Shop> = mutableListOf()
)